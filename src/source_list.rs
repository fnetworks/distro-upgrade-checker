use std::collections::HashMap;
use std::fs;
use std::io::{BufRead, BufReader, Read};
use std::path::PathBuf;
use url::Url;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SourceType {
    Deb,
    DebSource,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Source {
    pub srctype: SourceType,
    pub options: HashMap<String, String>,
    pub uri: Url,
    pub suite: String,
    pub components: Vec<String>,
}

#[derive(Debug, Clone)]
pub struct SourceList {
    pub sources: Vec<Source>,
}

impl SourceList {
    pub fn parse_system_sources() -> std::io::Result<Vec<Self>> {
        let mut files = vec![];
        files.push(PathBuf::from("/etc/apt/sources.list"));
        if let Ok(dirs) = fs::read_dir("/etc/apt/sources.list.d/") {
            for dir in dirs {
                let dir: std::fs::DirEntry = dir.unwrap();
                let path = dir.path();
                if let Some(extension) = path.extension() {
                    if extension == "list" {
                        files.push(path);
                    }
                }
            }
        }

        let mut result = vec![];
        for file in files {
            result.push(Self::parse_stream(&mut fs::File::open(file)?)?);
        }
        Ok(result)
    }

    pub fn parse_stream(read: &mut impl Read) -> std::io::Result<Self> {
        let read = BufReader::new(read);
        let mut sources: Vec<Source> = vec![];
        for line in read.lines() {
            let line = line?;
            let line: &str = line.trim_start();
            if line.is_empty() || line.starts_with("#") {
                continue;
            }
            let line = line.splitn(2, "#").nth(0).unwrap().trim_end();

            let mut tokens = line.split(" ").peekable();

            let source_type = tokens.next().unwrap();
            match source_type {
                "deb" | "deb-src" => {
                    let opts_result = if tokens.peek().unwrap().starts_with("[") {
                        let next = tokens.next().unwrap();
                        let options = next[1..next.len() - 1].split(",").map(|e| e.splitn(2, "="));
                        let mut opts_result = HashMap::new();
                        for mut option in options {
                            let key = option.next().unwrap();
                            let value = option.next().unwrap();
                            opts_result.insert(key.to_owned(), value.to_owned());
                        }

                        opts_result
                    } else {
                        HashMap::new()
                    };
                    let uri = tokens.next().unwrap();
                    let uri = if !uri.ends_with("/") {
                        format!("{}/", uri)
                    } else {
                        uri.to_owned()
                    };
                    let uri = Url::parse(&uri).unwrap();
                    let suite = tokens.next().unwrap();
                    let components = tokens.map(|e| e.to_owned()).collect::<Vec<_>>();
                    sources.push(Source {
                        srctype: if source_type == "deb-src" {
                            SourceType::DebSource
                        } else {
                            SourceType::Deb
                        },
                        options: opts_result,
                        uri,
                        suite: suite.to_owned(),
                        components,
                    });
                }
                _ => {
                    eprintln!("Unhandled source type {}", source_type);
                }
            }
        }
        Ok(Self { sources })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn test_parse() {
        let test_string = r#"
			deb http://example.org/linux disco main
			deb-src http://example.org/linux disco main
			deb [arch=amd64] http://example.org/linux disco main unstable
		"#;

        let parsed = SourceList::parse_stream(&mut Cursor::new(test_string)).unwrap();
        assert_eq!(parsed.sources.len(), 3);

        {
            let source = &parsed.sources[0];
            assert_eq!(source.srctype, SourceType::Deb);
            assert_eq!(source.uri.host_str(), Some("example.org"));
            assert_eq!(source.uri.path(), "/linux/");
            assert_eq!(source.suite, "disco");
            assert_eq!(source.components.len(), 1);
            assert_eq!(source.components[0], "main");
        }

        {
            let source = &parsed.sources[1];
            assert_eq!(source.srctype, SourceType::DebSource);
            assert_eq!(source.uri.host_str(), Some("example.org"));
            assert_eq!(source.uri.path(), "/linux/");
            assert_eq!(source.suite, "disco");
            assert_eq!(source.components.len(), 1);
            assert_eq!(source.components[0], "main");
        }

        {
            let source = &parsed.sources[2];
            assert_eq!(source.srctype, SourceType::Deb);
            assert_eq!(source.uri.host_str(), Some("example.org"));
            assert_eq!(source.uri.path(), "/linux/");
            assert_eq!(source.suite, "disco");
            assert_eq!(source.components.len(), 2);
            assert_eq!(source.components[0], "main");
            assert_eq!(source.components[1], "unstable");
        }
    }
}
