mod source_list;

use reqwest::Client;

fn main() {
    let target_distribution = std::env::args()
        .nth(1)
        .expect("usage: ubuntu-upgrade-check <distribution>");

    let sources = source_list::SourceList::parse_system_sources().unwrap();
    let all_sources = sources.into_iter().flat_map(|e| e.sources.into_iter());

    let mut distinct_uris = vec![];
    for source in all_sources {
        if !distinct_uris.contains(&source.uri) {
            distinct_uris.push(source.uri);
        }
    }

    let mut problem_repos: Vec<url::Url> = vec![];
    for source_uri in distinct_uris {
        let releases_uri = source_uri
            .join(&format!("dists/{}/Release", target_distribution))
            .unwrap();
        println!("Checking {} ...", source_uri);

        let response: reqwest::Response = Client::new()
            .head(&releases_uri.to_string())
            .send()
            .unwrap();
        if response.status() != 200 {
            problem_repos.push(source_uri);
        }
    }

    if problem_repos.len() > 0 {
        println!(
            "The following repositories seem to not have support for {}:",
            target_distribution
        );
        for repo in problem_repos {
            println!("{}", repo);
        }
        println!("Repositories listed above may still support {}, but they could use another suite name.", target_distribution);
    } else {
        println!(
            "It seems all your repositories support {}!",
            target_distribution
        );
    }
}
